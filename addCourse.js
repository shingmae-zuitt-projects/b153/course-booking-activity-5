let formSubmit= document.querySelector('#createCourse')
//console.log(registerForm)


formSubmit.addEventListener("submit", (e) => {
    e.preventDefault()
    
    let courseName = document.querySelector("#courseName").value
	let price = document.querySelector("#coursePrice").value
	let description = document.querySelector("#courseDescription").value

	fetch("http://localhost:4000/courses", {
		method: "POST",
		headers: {
			"Content-Type": "application/json",
			Authorizarion: `Bearer ${localStorage.getItem("token")}`
		},
		body: JSON.stringify({
			name: courseName,
			price: price,
			description: description
		})
	})
	.then(res => res.json())
	.then(data => {
		if(data === true){
			window.location.replace("./courses.html")
		}else{
			alert("Course not added. Please try again")
		}
	})
})